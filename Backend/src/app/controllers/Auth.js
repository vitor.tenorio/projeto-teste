import { Router } from 'express';
import bcrypt from 'bcryptjs';
import authConfig from '@/config/auth';
import crypto from 'crypto';
import jwt from 'jsonwebtoken';
import Mailer from '@/modules/Mailer';
import User from '@/app/schemas/User';

const router = new Router();

const generateToken = params => {
  return jwt.sign(params, authConfig.secret, { expiresIn: 86400 });
};

router.post('/register', (req, res) => {
  const { email, name, password } = req.body;

  User.findOne({ email })
    .then(userData => {
      if (userData) {
        return res.status(400).send({ error: 'Usuário já existe!' });
      } else {
        User.create({ name, email, password })
          .then(user => {
            user.password = undefined;
            return res.send({ user });
          })
          .catch(error => {
            console.error('Erro ao salvar usuário', error);
            return res.status(400).send({ error: 'Registration failed' });
          });
      }
    })
    .catch(error => {
      console.error('Erro ao consultar usuário no banco de dados', error);
      return res.status(500).send({ error: 'O registro falhou' });
    });
});

router.post('/login', (req, res) => {
  const { email, password } = req.body;

  User.findOne({ email })
    .select('+password')
    .then(user => {
      if (user) {
        bcrypt
          .compare(password, user.password)
          .then(result => {
            if (result) {
              const token = generateToken({ uid: user.id });
              return res.send({ token: token, tokenExpiratrion: '1d' });
            } else {
              return res.status(400).send({ error: 'Senha Inválida!' });
            }
          })
          .catch(error => {
            console.error('Erro ao verificar senha', error);
            return res.status(500).send({ error: 'Erro interno' });
          });
      } else {
        res.status(404).send({ error: 'Usuário não encontrado' });
      }
    })
    .catch(error => {
      console.error('Erro no login', error);
      return res.status(500).send({ error: 'Erro interno no servidor' });
    });
});

router.post('/forgot-password', (req, res) => {
  const { email } = req.body;

  User.findOne({ email })
    .then(user => {
      if (user) {
        const token = crypto.randomBytes(20).toString('hex');
        const expiration = new Date();
        expiration.setHours(new Date().getHours() + 3);

        User.findByIdAndUpdate(user.id, {
          $set: {
            passwordResetToken: token,
            passwordResetTokenExpiration: expiration,
          },
        })
          .then(() => {
            Mailer.sendMail(
              {
                to: email,
                from: 'webmaster@testeexpress.com',
                template: 'auth/forgot_password',
                context: { token },
              },
              error => {
                if (error) {
                  console.error('Erro ao enviar email', error);
                  return res.status(400).send({
                    error: 'Erro ao enviar email de recuperação de senha',
                  });
                } else {
                  return res.send();
                }
              },
            );
          })
          .catch(error => {
            console.error(
              'Erro ao salvar o token de recuperação de senha',
              error,
            );
            return res.status(500).send({ error: 'Erro interno do sistema' });
          });
      } else {
        return res.status(404).send({ error: 'Usuário não encontrado' });
      }
    })
    .catch(error => {
      console.error('Erro no esqueceu sua senha', error);
      return res.status(500).send({ error: 'Erro interno no servidor' });
    });
});

router.post('/reset-password', (req, res) => {
  const { email, token, newPassword } = req.body;
  User.findOne({ email })
    .select('+passwordResetToken passwordResetTokenExpiration')
    .then(user => {
      if (user) {
        if (
          token != user.passwordResetToken ||
          new Date().now > user.passwordResetTokenExpiration
        ) {
          return res.status(400).send({ error: 'Token inválido' });
        } else {
          user.passwordResetToken = undefined;
          user.passwordResetTokenExpiration = undefined;
          user.password = newPassword;

          user
            .save()
            .then(() => {
              return res.send({ message: 'Senha trocada com sucesso' });
            })
            .catch(error => {
              console.error('Erro ao salvar senha do usuário', error);
              return res.status(500).send({ error: 'Internal server error' });
            });
        }
      } else {
        res.status(404).send({ error: 'Usuário não encontrado' });
      }
    })
    .catch(error => {
      console.error('Erro no esqueceu sua senha', error);
      return res.status(500).send({ error: 'Erro interno no servidor' });
    });
});

export default router;
